
GNUTAR ?= tar

dev.tgz:
	$(GNUTAR) zcvf dev.tgz --owner=root --group=root --exclude-backups *DIRAC

dev.zip:
	zip -r dev.zip *DIRAC

